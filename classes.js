var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var Doctor = /** @class */ (function () {
    function Doctor() {
    }
    Doctor.prototype.treatPatients = function () {
        console.log("Treating patients");
    };
    return Doctor;
}());
var Ortho = /** @class */ (function (_super) {
    __extends(Ortho, _super);
    function Ortho() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    Ortho.prototype.treatPatients = function () {
        console.log("Treating patients with bone ailments");
    };
    Ortho.prototype.treatLikeNormalPatient = function () {
        _super.prototype.treatPatients.call(this);
    };
    return Ortho;
}(Doctor));
var aOrtho = new Ortho();
aOrtho.treatPatients();
