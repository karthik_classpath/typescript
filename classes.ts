export  class Doctor {
    
    constructor(private firstName,  private lastName){
    }

    getFirstName(){
        return  this.firstName;
    }
    setFirstName(firstName){
        this.firstName  =   firstName;
    }

    setLasstName(lastName){
        this.lastName  =   lastName;
    }
    getLastName(){
        return  this.lastName;
    }
    
    
    treatPatients(){
        console.log("Treating patients");
    }
    
}

class Ortho extends Doctor{
    treatPatients(){
        console.log("Treating patients with bone ailments");
    }
    treatLikeNormalPatient(){
        super.treatPatients();
    }
}

var aOrtho : Doctor =   new Ortho();
aOrtho.treatPatients();